package com.aria.common

object Resource {

  /**
   * Auto-closes a resource after use.
   *
   * Takes a given resource and a function using that resource.
   * After evaluating the function the "close" method on the resource is called.
   *
   * Example:
   * <code>
   *   <br>import com.aria.bicep.common.Resource._
   *   <br>val fileLines = using(io.Source.fromFile("myfile.txt"))(_.getLines)
   * </code>
   */
  def using[A <: {def close(): Unit}, B](resource: A)(f: A => B): B = {
    import scala.language.reflectiveCalls // needed for resource.close()

    try {
      f(resource)
    } finally {
      resource.close()
    }
  }
}

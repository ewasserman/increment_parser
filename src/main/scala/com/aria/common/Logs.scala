package com.aria.common

import org.slf4j.LoggerFactory

trait Logs {
  lazy val logger = LoggerFactory.getLogger(this.getClass.getName)

  protected var logPrefix: Option[String] = None

  private def prefix(msg: String) =
    logPrefix match {
      case Some(p) => logPrefix + msg
      case _ => msg
    }

  // Note because we use call-by-name (i.e. def f(x: => String))
  // the arguments won't be evaluated if log level is not enabled

  def trace(msg: => String): Unit = {
    if (logger.isTraceEnabled)
      logger.trace(prefix(msg))
  }

  def trace(msg: => String, e: => Throwable) = {
    if (logger.isTraceEnabled)
      logger.trace(prefix(msg),e)
  }

  def debug(msg: => String): Unit = {
    if (logger.isDebugEnabled)
      logger.debug(prefix(msg))
  }

  def debug(msg: => String, e: => Throwable) = {
    if (logger.isDebugEnabled)
      logger.debug(prefix(msg),e)
  }

  def info(msg: => String): Unit = {
    if (logger.isInfoEnabled)
      logger.info(prefix(msg))
  }

  def info(msg: => String, e: => Throwable) = {
    if (logger.isInfoEnabled)
      logger.info(prefix(msg),e)
  }

  def warn(msg: => String): Unit = {
    if (logger.isWarnEnabled)
      logger.warn(prefix(msg))
  }

  def warn(msg: => String, e: => Throwable) = {
    if (logger.isWarnEnabled)
      logger.warn(prefix(msg),e)
  }

  def error(msg: => String): Unit = {
    if (logger.isErrorEnabled)
      logger.error(prefix(msg))
  }

  def error(msg: => String, e: => Throwable) = {
    if (logger.isErrorEnabled)
      logger.error(prefix(msg),e)
  }

}

package c.a

import c.a.IncrementalUtils._
import com.amazonaws.services.s3.AmazonS3Client
import com.aria.common.Logs

object AWSMain extends App with Logs {

  // Must use this S3 Endpoint to get read-after-write consistency in S3 US-Standard region
  val s3Endpoint = "s3-external-1.amazonaws.com"
  val s3Client = new AmazonS3Client()
  s3Client.setEndpoint(s3Endpoint)

  val S3_BUCKET = "com.aria.bi-dev"
  val DB_NAME = "ec01dub"

  val x:List[IncrementDefinition] = getIncrementDefinitions(s3Client, S3_BUCKET, DB_NAME, 0)
  printIncrements(x)

}

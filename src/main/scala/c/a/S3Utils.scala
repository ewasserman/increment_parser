package c.a

import java.io.IOException

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.{ListObjectsRequest, ObjectListing, S3ObjectSummary}
import com.aria.common.Logs
import com.aria.common.Resource._

import scala.collection.JavaConversions.{collectionAsScalaIterable => asScala}

object S3Utils extends Logs {

  /**
   * Map a function over the keys in an S3 bucket
   * @param s3 a S3 Java SDK client
   * @param bucket the name of the bucket
   * @param prefix the prefix of the keys
   * @param marker if present, indicates where to begin listing. Include keys that occur lexicographically after the marker.
   * @param delimiter if present, treat like a hierarchical file system with specified delimiter. Include only objects in the 'prefix' directory
   * @param f function to map over the list
   * @tparam T the return type of the function
   * @return an iterable over the T's
   */
  def mapOverS3[T](s3: AmazonS3Client, bucket: String, prefix: String, marker: Option[String] = None, delimiter: Option[String] = None)(f: (S3ObjectSummary) => T) = {

    def scan(listing:ObjectListing): List[T] = {
      val summaries = asScala[S3ObjectSummary](listing.getObjectSummaries)
      val mapped = (for (summary <- summaries) yield f(summary)).toList

      if (!listing.isTruncated)  mapped.toList
      else mapped ::: scan(s3.listNextBatchOfObjects(listing))
    }

    val request = new ListObjectsRequest().withBucketName(bucket).withPrefix(prefix)
    marker.foreach(request.withMarker)
    delimiter.foreach(request.withDelimiter)
    scan(s3.listObjects(request))
  }

  /**
   * Reads an S3 object's contents into a string
   * @param s3 the initialized s3 client
   * @param bucket the bucket to read
   * @param key the key of the object to read
   * @return a string that is the content of the object
   */
  def getObjectAsString(s3: AmazonS3Client, bucket: String, key: String): Option[String] = {
    try {
      val obj = s3.getObject(bucket, key)
      val result = using(obj.getObjectContent)(scala.io.Source.fromInputStream(_).mkString)
      Some(result)
    } catch {
      case e: IOException =>
        error(s"error reading object from S3. ${e.getMessage}")
        None
    }
  }
}

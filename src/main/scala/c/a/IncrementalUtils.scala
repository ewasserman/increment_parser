package c.a

import c.a.S3Utils._
import com.amazonaws.services.s3.AmazonS3Client
import com.aria.common.Logs

import scala.collection.immutable.Iterable
import scala.util.{Failure, Success, Try}

object IncrementalUtils extends Logs {

  /**
   * Utility for printing a sequence of increments to the console
   * @param x the increments
   */
  def printIncrements(x: Iterable[IncrementDefinition]) = {
    x.foreach {
      id =>
        println(s"SCN: ${id.scn}")
        id.tableToUrls.foreach {
          tbl2Urls =>
            println(s"  Table: ${tbl2Urls._1}")
            tbl2Urls._2.foreach {
              url =>
                println(s"    $url")
            }
        }
    }
  }

  /**
   * A list of S3 paths to load for a table
   * @param table the name of the source table
   * @param s3Urls the S3 URLs to the files with data for the table
   */
  type TableUrlMap = Map[String, Set[String]]

  /**
   * The S3 paths for an increment with a given last SCN organized by table
   * @param scn the highest (last) SCN in the increment
   * @param tableToUrls the increment's per-table S3 source URLs
   */
  case class IncrementDefinition(scn: Long, tableToUrls: TableUrlMap)

  def urlsToTableUrlMap(urls: Set[String]): TableUrlMap = {
    val tableToUrls = urls.map(u => (urlToTableName(u), u))
    tableToUrls.groupBy(_._1).mapValues(e => e.map(_._2).toSet)
  }

  /**
   * Get all the S3 paths for all the increments for a given source database that are ready to load.
   *
   * Results are sorted ascending by SCN, and then table name.
   *
   * Find all the 'commit' files in the S3 bucket for which SCN > lastSCN.
   * Load each of these commit files to form an IncrementDefinition.
   * Sort by ascending SCN.
   *
   * @param s3Client: initialized S3 client
   * @param bucket the S3 bucket name
   * @param database the database
   * @param lastScn the lower (exclusive) bound for commits
   * @return the list of increments sorted by ascending SCN
   */
  def getIncrementDefinitions(s3Client: AmazonS3Client, bucket: String, database: String, lastScn: Long): List[IncrementDefinition]  = {
    val prefix = s"inc/$database"
    val startAt = f"$prefix/${lastScn+1}%015d"
    val commitKeys = mapOverS3(s3Client, bucket, prefix, marker = Some(startAt))(s => s.getKey)
      .filter(o => o.endsWith("/commit"))
    debug(s"commitKeys (cnt=${commitKeys.size}) after prefix=$startAt: $commitKeys")

    // keep only commit files with SCN after last
    // ordered by SCN and then datetime ascending
    val commitParsedPaths: List[ParsedPath] = commitKeys
      .flatMap(k => parseIncrementFilePath(k))
      .filter(pp => pp.scn > lastScn)
      .sortBy(pp => (pp.scn, pp.dateTime))
    debug(s"commit files with SCN > $lastScn : ${commitParsedPaths.size}")

    // list of SCN's with associated URLs to load for each; an SCN may appear more than once
    val scnUrls: List[(Long, List[String])] = commitParsedPaths.map {
      pp =>
        val commitContent = getObjectAsString(s3Client, bucket, pp.key)
        val urls: List[String] = commitContent.getOrElse("").split("\n").toList
        (pp.scn, urls)
    }

    // gather URL's into sets that share the same SCN
    val scnToUrls: Map[Long, Set[String]] = scnUrls.groupBy(_._1).mapValues(v => v.flatMap(e => e._2).toSet)

    val scnToTableToUrl: Map[Long, TableUrlMap] = scnToUrls.map(e => (e._1 -> urlsToTableUrlMap(e._2)))
    scnToTableToUrl.map(e => IncrementDefinition(e._1, e._2)).toList.sortBy(_.scn)
  }



  /**
   * Extract the source table name from the contents of the last segment in the S3 URL
   * @param s3Url the S3 URL
   * @return the source table name
   */
  def urlToTableName(s3Url: String): String = {
    val regex = """[sS]3.*/([^/]+)[.]([^./]*)""".r
    s3Url match {
      case regex(name, ext) => name
      case _ => s3Url
    }
  }

  /**
   * An S3 Path that has been parsed
   * @param bucket the S3 bucket
   * @param database the database name
   * @param scn the parsed SCN as an integer Long
   * @param dateTime the date and time string (ISO 8601 without the - and : separators: 'YYYYMMDDTHHMMSSZ')
   * @param lastSegment the last segment in the URL (i.e. the "filename")
   * @param unparsedPath the input path that was parsed
   * @param key the "key" portion of the input path URL. This is everything after: "s3://bucket_name/"
   */
  case class ParsedPath(bucket: Option[String], database: String, scn: Long, dateTime: String, lastSegment: String, unparsedPath: String, key: String)

  /**
   * Parses an S3 path (full or just the "key") to an increment file
   * @param path either a full path or just the key portion of the path to an increment file
   *             example full: "s3://my.bucket/inc/mydatabase/9477575923/20150602T231134Z/ARIACORE.ACCT_TRANSACTION_.gz
   *             example relative: "inc/ec01qub/9477575923/20150602T231134Z/ARIACORE.ACCT_TRANSACTION_.gz"
   * @return the ParsedPath
   */
  def parseIncrementFilePath(path: String): Option[ParsedPath] = {
    val pathRegex = """(?:[sS]3://([^/]+)?/)?(inc/([^/]+)/([^/]+)/(\d{8}T\d{6}Z)/([^/]+))""".r
    path match {
      case pathRegex(bucket, key, database, scnStr, datetime, lastSegmentStr) =>
        val scn = Try(scnStr.toLong)
        scn match {
          case Success(scn) => Some(ParsedPath(Option(bucket), database, scn, datetime, lastSegmentStr, path, key))
          case Failure(_) =>
            error(s"S3 key '$path' does not contain a valid SCN")
            None
        }
      case _ =>
        None
    }
  }

}


name := "Incremental Load Parser"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "ch.qos.logback"      % "logback-classic"   % "1.1.2", // Logback with slf4j facade
  "com.amazonaws" % "aws-java-sdk" % "1.10.2"
)
